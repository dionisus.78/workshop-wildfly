## **instalación de wildfly en ubuntu 22.04**


**Actualizar el sistema**
Primero, deberá actualizar todos los paquetes de su sistema a la versión actualizada. Puede actualizarlos todos ejecutando el siguiente comando.

```
apt update -y
apt upgrade -y
```
Una vez que se actualicen todos los paquetes, puede continuar con el siguiente paso.

**Instalar Java JDK**
Wildfly es una aplicación basada en Java. Por lo tanto, deberá instalar Java en su servidor. Puede instalar Java JDK con el siguiente comando.

`apt install default-jdk -y`
Una vez instalado Java, puede verificar la versión de Java mediante el siguiente comando.

`java --version`
Debería ver la versión de Java en la siguiente salida.

```
openjdk 11.0.17 2022-10-18
OpenJDK Runtime Environment (build 11.0.17+8-post-Ubuntu-1ubuntu222.04)
OpenJDK 64-Bit Server VM (build 11.0.17+8-post-Ubuntu-1ubuntu222.04, mixed mode, sharing)
```
**Descargue e instale Wildfly**
Antes de comenzar, deberá crear un usuario y un grupo dedicados para ejecutar Wildfly. Puede crear ambos mediante el siguiente comando.

```
groupadd -r wildfly
useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly
```
A continuación, descargue la última versión de Wildfly usando el siguiente comando.
`wget https://github.com/wildfly/wildfly/releases/download/28.0.1.Final/wildfly-28.0.1.Final.zip`
Una vez completada la descarga, descomprima el archivo descargado con el siguiente comando.

`unzip wildfly-28.0.1.Final.zip`
A continuación, mueva el directorio Wildfly extraído al directorio /opt.

`mv wildfly-28.0.1.Final.zip /opt/wildfly`
A continuación, cambie la propiedad del directorio Wildfly mediante el siguiente comando.

`chown -R wildfly.wildfly /opt/wildfly`
Una vez que haya terminado, puede proceder a configurar Wildfly.

`Configurar Wildfly`
En primer lugar, cree un directorio de configuración de Wildfly mediante el siguiente comando.
`mkdir -p /etc/wildfly`
A continuación, copie el archivo de configuración de Wildfly en el directorio /etc/wildfly.

`cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/`
A continuación, copie el archivo de servicio Wildfly en el directorio systemd.

`cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/`
A continuación, copie el archivo  **launch.sh** Wildfly en el directorio /opt/wildfly/bin/.

`cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/`
A continuación, establezca el permiso ejecutable en el archivo de script.

`chmod +x /opt/wildfly/bin/*.sh`
A continuación, vuelva a cargar el demonio systemd para aplicar los cambios.

```
systemctl daemon-reload
```
A continuación, inicie el servicio wildfly y agréguelo al arranque del sistema.

```
systemctl start wildfly
systemctl enable wildfly
```
Ahora puede verificar el estado de Wildfly usando el siguiente comando.

`systemctl status wildfly`
Obtendrá el siguiente resultado.

```
? wildfly.service - The WildFly Application Server
     Loaded: loaded (/etc/systemd/system/wildfly.service; disabled; vendor preset: enabled)
     Active: active (running) since Wed 2023-02-15 09:34:24 UTC; 4s ago
   Main PID: 20425 (launch.sh)
      Tasks: 61 (limit: 4579)
     Memory: 111.6M
        CPU: 6.919s
     CGroup: /system.slice/wildfly.service
             ??20425 /bin/bash /opt/wildfly/bin/launch.sh standalone standalone.xml 0.0.0.0
             ??20426 /bin/sh /opt/wildfly/bin/standalone.sh -c standalone.xml -b 0.0.0.0
             ??20534 java "-D[Standalone]" -server -Xms64m -Xmx512m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stac>

Feb 15 09:34:24 ubuntu2204 systemd[1]: Started The WildFly Application Server.
```
De forma predeterminada, Wildfly escucha en los puertos 8080 y 9990. Puede verificarlos con el siguiente comando.

`ss -antpl | grep -i java`
Debería ver el siguiente resultado.

```
LISTEN 0      4096         0.0.0.0:8443      0.0.0.0:*    users:(("java",pid=20534,fd=498))                                                                
LISTEN 0      50         127.0.0.1:9990      0.0.0.0:*    users:(("java",pid=20534,fd=497))                                                                
LISTEN 0      4096         0.0.0.0:8080      0.0.0.0:*    users:(("java",pid=20534,fd=495))
```                                                                
**Configurar la consola de administración de Wildfly**
De forma predeterminada, la consola de administración de Wildfly está deshabilitada. Por lo tanto, deberá habilitarlo para acceder a Wildfly desde el navegador web.

Primero, edite el archivo de configuración de Wildfly usando el siguiente comando.

`nano /etc/wildfly/wildfly.conf`
Cambie las siguientes líneas.

```
WILDFLY_BIND=127.0.0.1
WILDFLY_CONSOLE_BIND=127.0.0.1
```
Guarde y cierre el archivo cuando haya terminado. A continuación, edite el archivo launch.sh con el siguiente comando.

`nano /opt/wildfly/bin/launch.sh`
Modifique las líneas siguientes:

```
if [[ "$1" == "domain" ]]; then
    $WILDFLY_HOME/bin/domain.sh -c $2 -b $3 -bmanagement $4
else
    $WILDFLY_HOME/bin/standalone.sh -c $2 -b $3 -bmanagement $4
fi
```
Guarde y cierre el archivo y, a continuación, edite el archivo de servicio Wildfly.

`nano /etc/systemd/system/wildfly.service`
Modifique la línea siguiente.

`ExecStart=/opt/wildfly/bin/launch.sh $WILDFLY_MODE $WILDFLY_CONFIG $WILDFLY_BIND $WILDFLY_CONSOLE_BIND`
Guarde y cierre el archivo y vuelva a cargar el demonio systemd para aplicar los cambios.

`systemctl daemon-reload`
Ahora, reinicie el servicio Wildfly para implementar los cambios.

`systemctl restart wildfly`
**Agregar usuario administrativo de Wildfly**
A continuación, deberá agregar un usuario administrador para acceder a la consola de administración de Wildfly. Puede agregarlo con el siguiente comando.

`sh /opt/wildfly/bin/add-user.sh`
Se le pedirá el tipo de usuario que desea agregar:

What type of user do you wish to add? 
 ```
a) Management User (mgmt-users.properties) 
 b) Application User (application-users.properties)
(a): a
```
Proporcione los nuevos datos de usuario como se muestra a continuación:

```
Using the realm 'ManagementRealm' as discovered from the existing property files.

Username : wadmin
Password recommendations are listed below. To modify these restrictions edit the add-user.properties configuration file.
 - The password should be different from the username
 - The password should not be one of the following restricted values {root, admin, administrator}
 - The password should contain at least 8 characters, 1 alphabetic character(s), 1 digit(s), 1 non-alphanumeric symbol(s)
Password : 
Re-enter Password :
```

Se le preguntará a qué grupo desea que pertenezca este usuario:

```
What groups do you want this user to belong to? (Please enter a comma separated list, or leave blank for none)[  ]:
Simplemente presione la tecla Enter. Debería ver el siguiente resultado.

About to add user 'wadmin' for realm 'ManagementRealm'
Is this correct yes/no? yes
Added user 'wadmin' to file '/opt/wildfly/standalone/configuration/mgmt-users.properties'
Added user 'wadmin' to file '/opt/wildfly/domain/configuration/mgmt-users.properties'
Added user 'wadmin' with groups  to file '/opt/wildfly/standalone/configuration/mgmt-groups.properties'
Added user 'wadmin' with groups  to file '/opt/wildfly/domain/configuration/mgmt-groups.properties'
Is this new user going to be used for one AS process to connect to another AS process? 
e.g. for a slave host controller connecting to the master or for a Remoting connection for server to server EJB calls.
yes/no? yes
To represent the user add the following to the server-identities definition
``` 
**Configurar Nginx para Wildfly**
En este punto, Wildfly está instalado y configurado para ejecutarse en localhost. Ahora, deberá configurar Nginx como un proxy inverso para acceder a la interfaz web de Wildfly.

Primero, instale el paquete Nginx usando el siguiente comando.

`apt install nginx -y`
A continuación, cree un archivo de configuración proxy_headers.

`nano /etc/nginx/conf.d/proxy_headers.conf`
Agregue las siguientes configuraciones:

```
proxy_set_header Host $host;
proxy_set_header X-Forwarded-Proto $scheme;
add_header Front-End-Https on;
add_header Cache-Control no-cache;
```
Guarde y cierre el archivo y, a continuación, cree un nuevo archivo de configuración de host virtual.

`nano /etc/nginx/conf.d/wildfly.conf`
Agregue las siguientes configuraciones.

```
server {
  listen          80;
  server_name     wildfly.example.com;

  location / {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:8080;
  }

  location /management {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/management;
  }

  location /console {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/console;
  }

  location /logout {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/logout;
  }

  location /error {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990;
  }

}
```
Guarde el archivo y luego verifique el Nginx para detectar cualquier error de configuración de sintaxis.

`nginx -t`
Si todo está bien, obtendrá la siguiente salida.

nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
Finalmente, reinicie el servicio Nginx para implementar los cambios.

`systemctl restart nginx`
Puede verificar el estado del servicio Nginx con el siguiente comando.

`systemctl status nginx`
Debería ver el siguiente resultado.

```
? nginx.service - A high performance web server and a reverse proxy server
     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2023-02-15 09:38:18 UTC; 1s ago
       Docs: man:nginx(8)
    Process: 21027 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
    Process: 21028 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
   Main PID: 21029 (nginx)
      Tasks: 3 (limit: 4579)
     Memory: 3.4M
        CPU: 62ms
     CGroup: /system.slice/nginx.service
             ??21029 "nginx: master process /usr/sbin/nginx -g daemon on; master_process on;"
             ??21030 "nginx: worker process" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" ""
             ??21031 "nginx: worker process" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" "" ""

Feb 15 09:38:18 ubuntu2204 systemd[1]: Starting A high performance web server and a reverse proxy server...
Feb 15 09:38:18 ubuntu2204 systemd[1]: Started A high performance web server and a reverse proxy server.
```
**Acceda a la interfaz de administración de Wildfly**

**http://wildfly.example.com/console**
